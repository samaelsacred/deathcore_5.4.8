/*
 * Copyright (C) 2013-2015 DeathCore <http://www.noffearrdeathproject.net/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptPCH.h"
#include "hour_of_twilight.h"

enum Adds
{
    NPC_TWILIGHT_DRAKE              = 55636,
    NPC_HARBRINGER_OF_TWILIGHT      = 55969,
    NPC_CHAMPION_OF_LIFE            = 55911,
    NPC_HARBRINGER_OF_DESTRUCTION   = 55967,
    NPC_CHAMPION_OF_TIME            = 55913,
    NPC_DARK_HAZE                   = 54628, // 102287  102255
    NPC_FLAILING_TENTACLE           = 54696,
    NPC_BLOOD                       = 54644,
    NPC_THROW_LOC                   = 54735,
    NPC_SHADOW_BORER                = 54686,
    NPC_TWILIGHT_PORTAL             = 58233,

    NPC_EARTHEN_SHELL_TARGET        = 55445,
};

class npc_hour_of_twilight_life_warden : public CreatureScript
{
    public:
        npc_hour_of_twilight_life_warden() : CreatureScript("npc_hour_of_twilight_life_warden") { }

        bool OnGossipHello(Player* pPlayer, Creature* pCreature)
        {
            if (pPlayer->isInCombat())
                return true;

            pPlayer->NearTeleportTo(teleportPos.GetPositionX(), teleportPos.GetPositionY(), teleportPos.GetPositionZ(), teleportPos.GetOrientation());

            return true;
        }
};

void AddSC_hour_of_twilight()
{
    new npc_hour_of_twilight_life_warden();
}