/*
 * Copyright (C) 2013-2015 DeathCore <http://www.noffearrdeathproject.net/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEF_THRONEOFTHEFOURWINDS_H
#define DEF_THRONEOFTHEFOURWINDS_H

#define TotFWScriptName "instance_throne_of_the_four_winds"

enum Data
{
    DATA_CONCLAVE_OF_WIND,
    DATA_ALAKIR,
    DATA_ANSHAL,
    DATA_NEZIR,
    DATA_ROHASH,
};

enum Creatures
{
    NPC_ANSHAL  = 45870,
    NPC_NEZIR   = 45871,
    NPC_ROHASH  = 45872,
    NPC_ALAKIR  = 46753,
};

const Position startPos = {-266.834991f, 816.938049f, 200.0f, 0.0f};
const Position rohashPos = {-51.4635f, 576.25f, 200.1f, 1.51f}; // for tornado

#endif
