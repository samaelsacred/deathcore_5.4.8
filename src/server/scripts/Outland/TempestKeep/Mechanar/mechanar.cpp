/*
 * Copyright (C) 2013-2015 DeathCore <http://www.noffearrdeathproject.net/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "mechanar.h"

class gatewatcher_gyro_kill : public CreatureScript
{
    public:
        gatewatcher_gyro_kill() : CreatureScript("gatewatcher_gyro_kill") {}

        struct gatewatcher_gyro_killAI : public ScriptedAI
        {
            gatewatcher_gyro_killAI(Creature* creature) : ScriptedAI(creature)
            {
                pInstance = creature->GetInstanceScript();
            }

            InstanceScript* pInstance;

            void Reset()
            {
                if (pInstance)
                {
                    if (GameObject* pGob = pInstance->instance->GetGameObject(pInstance->GetData64(GOB_CACHE_LEGION)))
                        pGob->SetFlag(GAMEOBJECT_FLAGS, GO_FLAG_LOCKED);
                }
            }

            void JustDied(Unit* killer)
            {
                if (pInstance)
                    if (GameObject* pGob = pInstance->instance->GetGameObject(pInstance->GetData64(GOB_CACHE_LEGION)))
                            pGob->RemoveFlag(GAMEOBJECT_FLAGS, GO_FLAG_LOCKED);
            }

            void UpdateAI(const uint32 diff)
            {
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new gatewatcher_gyro_killAI(creature);
        }
};

void AddSC_mechanar()
{
    new gatewatcher_gyro_kill();
}

